package com.cultivator.xfly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XflyApplication {

	public static void main(String[] args) {
		SpringApplication.run(XflyApplication.class, args);
	}
}
